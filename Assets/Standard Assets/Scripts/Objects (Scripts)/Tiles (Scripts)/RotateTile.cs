﻿using UnityEngine;
using Extensions;
// #if UNITY_EDITOR
// using UnityEditor;
// #endif

namespace ArcherGame
{
	// [ExecuteInEditMode]
	public class RotateTile : UpdateWhileEnabled
	{
		public Transform trs;
		public float rotateSpeed;
		public bool switchDirectionsWhenHit;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				return;
			}
#endif
			base.OnEnable ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			trs.eulerAngles += Vector3.forward * rotateSpeed * Time.deltaTime;
		}

		void OnCollisionEnter2D (Collision2D coll)
		{
			if (switchDirectionsWhenHit)
				rotateSpeed *= -1;
		}
	}
}