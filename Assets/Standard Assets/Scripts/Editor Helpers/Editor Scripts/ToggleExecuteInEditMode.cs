#if UNITY_EDITOR
using System;
using UnityEngine;

public class ToggleExecuteInEditMode : EditorScript
{
	public ScriptEntry[] scriptEntries = new ScriptEntry[0];

	public override void Do ()
	{

	}

	[Serializable]
	public class ScriptEntry
	{
		public MonoBehaviour script;
	}
}
#else
using UnityEngine;

public class ToggleExecuteInEditMode : EditorScript
{
}
#endif