namespace ArcherGame
{
	public class AddToSkips : UpgradablePerk
	{
		public override void Buy ()
		{
			if (!Obelisk.playerIsAtObelisk)
			{
				if (SaveAndLoadManager.saveData.currentMoney >= cost)
					GameManager.Instance.notificationText.text.text = "You must be at an obelisk to buy perks!";
				else
					GameManager.Instance.notificationText.text.text = "You must be at an obelisk to buy perks! You don't have enough money anyway!";
				GameManager.Instance.notificationText.Do ();
				return;
			}
			if (SaveAndLoadManager.saveData.currentMoney >= cost)
				SkipManager.skipPoints ++;
			base.Buy ();
		}
	}
}