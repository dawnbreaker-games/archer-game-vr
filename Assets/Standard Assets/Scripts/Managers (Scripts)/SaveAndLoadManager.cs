﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
// using FullSerializer;
using ArcherGame;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[ExecuteInEditMode]
public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
{
	public static SaveData saveData;
	public static string MostRecentSaveFileName
	{
		get
		{
			return PlayerPrefs.GetString("Most recent save file name", null);
		}
		set
		{
			PlayerPrefs.SetString("Most recent save file name", value);
		}
	}
	// static fsSerializer serializer = new fsSerializer();
	
	public void Start ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		if (!string.IsNullOrEmpty(MostRecentSaveFileName))
			LoadMostRecent ();
	}

	void OnAboutToSave ()
	{
		Asset[] assets = FindObjectsOfType<Asset>(true);
		GameManager.instance.assetsData.Clear();
		for (int i = 0; i < assets.Length; i ++)
		{
			Asset asset = assets[i];
			asset.SetData ();
			GameManager.instance.assetsData.Add(asset._Data);
		}
	}
	
	public void Save (string fileName)
	{
		OnAboutToSave ();
		saveData.assetsData = GameManager.instance.assetsData.ToArray();
		// File.WriteAllText(fileName, Serialize(saveData, typeof(SaveData)));
		FileStream fileStream = new FileStream(fileName, FileMode.Create);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		binaryFormatter.Serialize(fileStream, saveData);
		fileStream.Close();
		MostRecentSaveFileName = fileName;
	}
	
	public void Load (string fileName)
	{
		// saveData = (SaveData) Deserialize(File.ReadAllText(fileName), typeof(SaveData));
		FileStream fileStream = new FileStream(fileName, FileMode.Open);
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
		fileStream.Close();
		OnLoad (fileName);
	}

	void OnLoad (string fileName)
	{
		GameManager.instance.assetsData = new List<Asset.Data>(saveData.assetsData);
		OnLoaded ();
		MostRecentSaveFileName = fileName;
	}

	void OnLoaded ()
	{
		List<Asset> assets = new List<Asset>();
		for (int i = 0; i < GameManager.instance.assetsData.Count; i ++)
		{
			Asset.Data assetData = GameManager.instance.assetsData[i];
			Asset correspondingAsset = null;
			for (int i2 = 0; i2 < assets.Count; i2 ++)
			{
				Asset asset = assets[i2];
				if (assetData.name == asset.name)
				{
					assets.RemoveAt(i2);
					correspondingAsset = asset;
					break;
				}
			}
			if (correspondingAsset == null)
				assetData.MakeAsset ();
			else
			{
				correspondingAsset.InitData ();
				if (correspondingAsset._Data.GetType() == assetData.GetType())
					assetData.Apply (correspondingAsset);
				else
				{
					ObjectPool.instance.Despawn (correspondingAsset.prefabIndex, correspondingAsset.gameObject, correspondingAsset.trs);
					assetData.MakeAsset ();
				}
			}
		}
	}
	
	public void LoadMostRecent ()
	{
		Load (MostRecentSaveFileName);
	}

	// public static string Serialize (object value, Type type)
	// {
	// 	fsData data;
	// 	serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();
	// 	return fsJsonPrinter.CompressedJson(data);
	// }
	
	// public static object Deserialize (string serializedState, Type type)
	// {
	// 	fsData data = fsJsonParser.Parse(serializedState);
	// 	object deserialized = null;
	// 	serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();
	// 	return deserialized;
	// }
	
	[Serializable]
	public struct SaveData
	{
		public Asset.Data[] assetsData;
	}
}
