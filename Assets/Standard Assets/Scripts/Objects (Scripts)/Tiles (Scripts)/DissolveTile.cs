using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class DissolveTile : MonoBehaviour
	{
		public Transform trs;
		public SpriteRenderer spriteRenderer;
		public new Collider2D collider;
		public Collider2D rebuildCollider;
		public float dissolveDuration;
		public float rebuildDelay;
		public float rebuildDuration;
		public LayerMask whatICantRebuildOn;
		// int numberOfCollidersTouchingMe;

		void OnCollisionEnter2D (Collision2D coll)
		{
			// numberOfCollidersTouchingMe ++;
			// if (numberOfCollidersTouchingMe == 1)
			// {
				StopAllCoroutines();
				if (spriteRenderer != null)
					StartCoroutine(DissolveRoutine_SpriteRenderer ());
			// }
		}

		// void OnCollisionExit2D (Collision2D coll)
		// {
		// 	numberOfCollidersTouchingMe --;
		// 	if (numberOfCollidersTouchingMe == 0)
		// 	{
		// 		StopAllCoroutines();
		// 		if (spriteRenderer != null)
		// 			StartCoroutine(RebuildRoutine_SpriteRenderer ());
		// 		else
		// 			StartCoroutine(RebuildRoutine_Terrain ());
		// 	}
		// }
		
		IEnumerator DissolveRoutine_SpriteRenderer ()
		{
			do
			{
				yield return new WaitForEndOfFrame();
				spriteRenderer.color = spriteRenderer.color.AddAlpha(-Time.deltaTime / dissolveDuration);
				if (spriteRenderer.color.a <= 0)
				{
					spriteRenderer.color = spriteRenderer.color.SetAlpha(0);
					break;
				}
			} while (true);
			collider.enabled = false;
			StartCoroutine(RebuildRoutine_SpriteRenderer ());
		}

		IEnumerator RebuildRoutine_SpriteRenderer ()
		{
			yield return new WaitForSeconds(rebuildDelay);
			ContactFilter2D contactFilter = new ContactFilter2D();
			contactFilter.layerMask = whatICantRebuildOn;
			contactFilter.useLayerMask = true;
			yield return new WaitUntil(() => (Physics2D.OverlapCollider(rebuildCollider, contactFilter, new Collider2D[1]) == 0));
			collider.enabled = true;
			while (true)
			{
				yield return new WaitForEndOfFrame();
				spriteRenderer.color = spriteRenderer.color.AddAlpha(Time.deltaTime / rebuildDuration);
				if (spriteRenderer.color.a >= 1)
				{
					spriteRenderer.color = spriteRenderer.color.SetAlpha(1);
					break;
				}
			}
		}
	}
}