using Extensions;
using UnityEngine;

namespace ArcherGame
{
	public class Player : PlatformerEntity
	{
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public bool canShoot;
	}
}