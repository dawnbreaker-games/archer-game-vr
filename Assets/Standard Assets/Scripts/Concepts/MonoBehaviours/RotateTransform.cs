using UnityEngine;
using Extensions;
using ArcherGame;

public class RotateTransform : UpdateWhileEnabled
{
	public float rate;
	public Transform trs;

	public override void DoUpdate ()
	{
		trs.eulerAngles += Vector3.forward * rate * Time.deltaTime;
	}
}