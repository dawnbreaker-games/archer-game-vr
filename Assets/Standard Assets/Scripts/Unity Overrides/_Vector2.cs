﻿using UnityEngine;

public class _Vector2
{
	public static _Vector2 zero = new _Vector2(0, 0);
	public float x;
	public float y;

	public _Vector2 (float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public _Vector2 (Vector2 v)
	{
		x = v.x;
		y = v.y;
	}

	public Vector2 ToVec2 ()
	{
		return new Vector2(x, y);
	}
}