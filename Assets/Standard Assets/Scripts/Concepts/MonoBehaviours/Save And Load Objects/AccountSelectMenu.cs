﻿using UnityEngine;
using Extensions;

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class AccountSelectMenu : SingletonMonoBehaviour<AccountSelectMenu>
	{
		public AccountSelectMenuOption[] menuOptions;
		public CanvasGroup canvasGroup;

		public static void Init ()
		{
			if (VirtualKeyboard.Instance != null)
				VirtualKeyboard.instance.DisableInput ();
			int numberOfEmptyAccounts = 0;
			for (int i = 0; i < Instance.menuOptions.Length; i ++)
			{
				AccountSelectMenuOption accountSelectMenuOption = instance.menuOptions[i];
				string accountName = SaveAndLoadManager.GetAccountName(accountSelectMenuOption.rectTrs.GetSiblingIndex());
				if (string.IsNullOrEmpty(accountName))
				{
					numberOfEmptyAccounts ++;
					accountSelectMenuOption.accountNameText.text = "Empty Account #" + numberOfEmptyAccounts;
				}
				else
				{
					accountSelectMenuOption.accountNameText.text = "Account: " + accountName;
					accountSelectMenuOption.createButtonGo.SetActive(false);
					accountSelectMenuOption.playButtonGo.SetActive(true);
					accountSelectMenuOption.startCopyButtonGo.SetActive(true);
					accountSelectMenuOption.deleteButtonGo.SetActive(true);
				}
			}
		}

#if UNITY_EDITOR
		public virtual void Start ()
		{
			if (!Application.isPlaying)
			{
				menuOptions = GetComponentsInChildren<AccountSelectMenuOption>();
				return;
			}
		}
#endif
	}
}
