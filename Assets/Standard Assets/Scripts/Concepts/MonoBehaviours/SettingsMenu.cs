using UnityEngine;
using Extensions;
using TMPro;
using UnityEngine.UI;

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class SettingsMenu : SingletonMonoBehaviour<SettingsMenu>
	{
		public GameObject go;
		public Transform trs;

		public void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (go == null)
					go = GetComponent<GameObject>();
				if (trs == null)
					trs = GetComponent<Transform>();
				return;
			}
#endif
		}

		public void Init ()
		{
			
		}

		public void SetMute (bool mute)
		{
			AudioListener.pause = mute;
		}

		public void SetVolume (float volume)
		{
			AudioListener.volume = volume;
		}

		public void SetVSyncCount (int vSyncCount)
		{
			QualitySettings.vSyncCount = vSyncCount;
		}

		public void SetAntiAliasingValue (int antiAliasingValue)
		{
			QualitySettings.antiAliasing = antiAliasingValue;
		}

		public void SetQualityLevel (int index)
		{
			QualitySettings.SetQualityLevel(index);
		}

		public void SetFullscreen (bool fullscreen)
		{
			Screen.fullScreen = fullscreen;
		}
	}
}