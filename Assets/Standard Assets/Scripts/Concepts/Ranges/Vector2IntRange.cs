using UnityEngine;
using Extensions;

public class Vector2IntRange : Range<Vector2Int>
{
	public Vector2IntRange (Vector2Int min, Vector2Int max) : base (min, max)
	{
	}
}