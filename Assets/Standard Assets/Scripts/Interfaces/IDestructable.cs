﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ArcherGame
{
	public interface IDestructable
	{
		float Hp { get; set; }
		uint MaxHp { get; set; }
		
		void TakeDamage (float amount);
		void Death ();
	}
}