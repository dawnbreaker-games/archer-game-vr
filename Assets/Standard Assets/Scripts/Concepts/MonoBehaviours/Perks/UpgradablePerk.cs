namespace ArcherGame
{
	public class UpgradablePerk : Perk
	{
		public int TimesPurchased
		{
			get
			{
				return SaveAndLoadManager.GetValue<int>(name + " times purchased", 0);
			}
			set
			{
				SaveAndLoadManager.SetValue (name + " times purchased", value);
			}
		}

		public override void Init ()
		{
			for (int i = 0; i < TimesPurchased; i ++)
				Apply ();
		}

		public override void Buy ()
		{
			if (!Obelisk.playerIsAtObelisk)
			{
				if (SaveAndLoadManager.saveData.currentMoney >= cost)
					GameManager.Instance.notificationText.text.text = "You must be at an obelisk to buy perks!";
				else
					GameManager.Instance.notificationText.text.text = "You must be at an obelisk to buy perks! You don't have enough money anyway!";
				GameManager.Instance.notificationText.Do ();
				return;
			}
			if (SaveAndLoadManager.saveData.currentMoney >= cost)
				TimesPurchased ++;
			base.Buy ();
		}
	}
}