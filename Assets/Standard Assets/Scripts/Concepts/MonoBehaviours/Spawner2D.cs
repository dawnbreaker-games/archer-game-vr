using UnityEngine;
using Extensions;
using ArcherGame;
using System;
using Random = UnityEngine.Random;

public class Spawner2D : MonoBehaviour
{
	public int prefabIndex;
	public int initSpawns;
	public SpawnZone2D[] spawnZones = new SpawnZone2D[0];
	public Timer spawnTimer;
	public float prefabRadius;
	public LayerMask whatICantSpawnIn;

	void Awake ()
	{
		for (int i = 0; i < initSpawns; i ++)
			Spawn ();
		spawnTimer.Reset ();
	}

	void OnEnable ()
	{
		spawnTimer.onFinished += Spawn;
		spawnTimer.Start ();
	}

	void Spawn (params object[] args)
	{
		while (true)
		{
			SpawnZone2D spawnZone = spawnZones[Random.Range(0, spawnZones.Length)];
			Vector2 destination = spawnZone.boxCollider.GetRect().RandomPoint();
			Vector2 point = spawnZone.points[Random.Range(0, spawnZone.points.Length)].position;
			Vector2 toDestination = destination - point;
			float distanceToDestination = toDestination.magnitude;
			RaycastHit2D hit = Physics2D.CircleCast(point, prefabRadius, toDestination, distanceToDestination, whatICantSpawnIn);
			if (hit.collider != null)
			{
				distanceToDestination = Mathf.Clamp(hit.distance - prefabRadius, 0, hit.distance);
				// if (distanceToDestination == 0)
				// 	continue;
			}
			ObjectPool.Instance.SpawnComponent<ISpawnable>(prefabIndex, point + (toDestination.normalized * distanceToDestination * Random.value), Quaternion.LookRotation(Vector3.forward, Random.insideUnitCircle.normalized));
			return;
		}
	}

	void OnDisable ()
	{
		spawnTimer.Stop ();
		spawnTimer.onFinished -= Spawn;
	}

	[Serializable]
	public struct SpawnZone2D
	{
		public BoxCollider2D boxCollider;
		public Transform[] points;
	}
}