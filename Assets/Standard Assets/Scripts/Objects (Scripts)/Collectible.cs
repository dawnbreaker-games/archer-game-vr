﻿using Extensions;
using UnityEngine;

namespace ArcherGame
{
	public class Collectible : MonoBehaviour
	{
		public static Collectible[] instances = new Collectible[0];
		public bool CollectedAndSaved
		{
			get
			{
				return SaveAndLoadManager.GetValue<bool>(name + " collected and saved", false);
			}
			set
			{
				SaveAndLoadManager.SetValue (name + " collected and saved", value);
			}
		}
		public bool collected;

		public virtual void Awake ()
		{
			instances = instances.Add(this);
		}

		public virtual void OnDestroy ()
		{
			instances = instances.Remove(this);
		}

		public virtual void OnTriggerEnter2D (Collider2D other)
		{
			OnCollected ();
			collected = true;
		}

		public virtual void OnCollected ()
		{
			gameObject.SetActive(false);
		}
	}
}