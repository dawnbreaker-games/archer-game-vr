﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Extensions;
using ArcherGame;

//[ExecuteInEditMode]
public class VirtualKey : UpdateWhileEnabled
{
	public Text text;
	public Outline outline;
	public Image image;
	bool isSelected;
	[HideInInspector]
	public Color notSelectedColor;
	public Color selectedColor;
	[HideInInspector]
	public Color notPressedColor;
	public Color pressedColor;
	public bool isActivatable;
	[HideInInspector]
	public bool isActivated;
	public Color activeColor;
	public UnityEvent invokeOnPressed;
	public UnityEvent invokeOnReleased;
	[HideInInspector]
	public string normalText;
	public bool alternateTextSameAsNormal;
	public string alternateText;
	[HideInInspector]
	public bool isPressed;
	public _Selectable selectable;

	void OnValidate ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			notSelectedColor = outline.effectColor;
			notPressedColor = image.color;
			normalText = text.text;
			if (alternateTextSameAsNormal)
				alternateText = normalText;
			return;
		}
#endif
	}

	public void Selected ()
	{
		outline.effectColor = selectedColor;
	}

	public void OnDeselected ()
	{
		outline.effectColor = notSelectedColor;
	}

	public void OnPressed ()
	{
		if (isPressed && isSelected)
			return;
		isPressed = true;
		image.color = pressedColor;
		if (invokeOnPressed != null)
			invokeOnPressed.Invoke(); 
	}

	public void OnReleased ()
	{
		if (!isPressed)
			return;
		isPressed = false;
		image.color = notPressedColor;
		if (isActivatable)
			ToggleActivate ();
		if (invokeOnReleased != null)
			invokeOnReleased.Invoke();
	}

	public void ToggleActivate ()
	{
		isActivated = !isActivated;
		if (isActivated)
			image.color = activeColor;
		else
			image.color = notPressedColor;
	}

	public void StartContinuousOutputToInputField ()
	{
		StartCoroutine(ContinuousOutputToInputFieldRoutine ());
	}

	IEnumerator ContinuousOutputToInputFieldRoutine ()
	{
		OutputToInputField ();
		yield return new WaitForSecondsRealtime(VirtualKeyboard.Instance.repeatDelay);
		while (true)
		{
			yield return new WaitForSecondsRealtime(VirtualKeyboard.instance.repeatRate);
			OutputToInputField ();
		}
	}

	public void OutputToInputField ()
	{
		if (VirtualKeyboard.Instance.outputToInputField.text.Length == VirtualKeyboard.instance.outputToInputField.characterLimit)
			return;
		VirtualKeyboard.instance.outputToInputField.text = VirtualKeyboard.instance.outputToInputField.text.Insert(VirtualKeyboard.instance.OutputPosition, text.text);
		VirtualKeyboard.instance.OutputPosition ++;
		if (VirtualKeyboard.instance.shiftKeys[0].isActivated)
		{
			VirtualKeyboard.instance.shiftKeys[0].ToggleActivate ();
			VirtualKeyboard.instance.OnShiftReleased (VirtualKeyboard.instance.shiftKeys[0]);
		}
	}

	public void EndContinuousOutputToInputField ()
	{
		StopAllCoroutines();
	}
}