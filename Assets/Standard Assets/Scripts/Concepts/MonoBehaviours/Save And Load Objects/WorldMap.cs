﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Extensions;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using DialogAndStory;

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class WorldMap : SingletonMonoBehaviour<WorldMap>, IUpdatable
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public Tilemap[] tilemaps = new Tilemap[0];
		public Tilemap unexploredTilemap;
		public static HashSet<_Vector2Int> exploredCellPositionsAtLastTimeOpened = new HashSet<_Vector2Int>();
		_Vector2Int cellPosition;
		// [HideInInspector]
		// public Vector2Int minExploredCellPosition;
		// [HideInInspector]
		// public Vector2Int maxExploredCellPosition;
		[HideInInspector]
		public Vector2Int minCellPosition;
		[HideInInspector]
		public Vector2Int maxCellPosition;
		Vector2Int previousMinCellPosition;
		public static List<WorldMapIcon> worldMapIcons = new List<WorldMapIcon>();
		public static bool isOpen;
		bool canControlCamera;
		public float cameraMoveSpeed;
		Vector2 moveInput;
		public float normalizedScreenBorder;
		Rect screenWithoutBorder;
		Obelisk fastTravelToObelisk;
		public TileBase unexploredTile;
		public WorldMapCamera worldMapCamera;
#if UNITY_EDITOR
		public bool update;
		public bool startOver;
		public int x;
		public int y;
#endif

		public void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (GameManager.Instance.doEditorUpdates)
					EditorApplication.update += DoEditorUpdate;
				return;
			}
			else
				EditorApplication.update -= DoEditorUpdate;
#endif
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				EditorApplication.update -= DoEditorUpdate;
				return;
			}
#endif
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

#if UNITY_EDITOR
		public void DoEditorUpdate ()
		{
			if (!update)
				return;
			if (startOver)
			{
				World.Instance.update = true;
				World.instance.DoEditorUpdate ();
				x = World.Instance.cellBoundsRect.xMin;
				y = World.Instance.cellBoundsRect.yMin;
				startOver = false;
			}
			unexploredTilemap.SetTile(new Vector3Int(x, y, 0), unexploredTile);
			if (x > World.Instance.cellBoundsRect.xMax + 1)
			{
				x = World.instance.cellBoundsRect.xMin;
				y ++;
				if (y > World.instance.cellBoundsRect.yMax + 1)
					update = false;
			}
			else
				x ++;
		}
#endif

		public void Init ()
		{
			if (!enabled)
				return;
			minCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.min).ToVec2Int();
			maxCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.max).ToVec2Int();
			if (SaveAndLoadManager.saveData.exploredCellPositions.Count == 0)
			{
				for (int x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					for (int y = minCellPosition.y; y <= maxCellPosition.y; y ++)
						SaveAndLoadManager.saveData.exploredCellPositions.Add(new _Vector2Int(x, y));
				}
				// minExploredCellPosition = minCellPosition;
				// maxExploredCellPosition = maxCellPosition;
			}
			// else
			// {
			// 	foreach (Vector2Int exploredCellPosition in SaveAndLoadManager.saveData.exploredCellPositions)
			// 	{
			// 		// minExploredCellPosition = VectorExtensions.SetToMinComponents(minExploredCellPosition, exploredCellPosition);
			// 		// maxExploredCellPosition = VectorExtensions.SetToMaxComponents(maxExploredCellPosition, exploredCellPosition);
			// 	}
			// }
			previousMinCellPosition = minCellPosition;
			screenWithoutBorder = new Rect();
			screenWithoutBorder.size = new Vector2(Screen.width - normalizedScreenBorder * Screen.width, Screen.height - normalizedScreenBorder * Screen.height);
			screenWithoutBorder.center = new Vector2(Screen.width / 2, Screen.height / 2);
			worldMapCamera.HandleViewSize ();
		}
		
		public void DoUpdate ()
		{
			if (!isOpen)
				UpdateExplored ();
			else
			{
				worldMapCamera.DoUpdate ();
				moveInput = InputManager.GetSwimInput(MathfExtensions.NULL_INT)+ InputManager.GetAimInput(MathfExtensions.NULL_INT);
				GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(true);
				if (InputManager.UsingGamepad)
				{
					GameManager.activeCursorEntry.rectTrs.position += (Vector3) moveInput * GameManager.cursorMoveSpeed * Time.unscaledDeltaTime;
					GameManager.activeCursorEntry.rectTrs.position = GameManager.activeCursorEntry.rectTrs.position.ClampComponents(Vector3.zero, new Vector2(Screen.width, Screen.height));
				}
				if (!screenWithoutBorder.Contains(GameManager.activeCursorEntry.rectTrs.position))
				{
					moveInput = (Vector2) GameManager.activeCursorEntry.rectTrs.position - new Vector2(Screen.width / 2, Screen.height / 2);
					moveInput /= new Vector2(Screen.width / 2, Screen.height / 2).magnitude;
					if (canControlCamera)
						worldMapCamera.trs.position += (Vector3) moveInput * cameraMoveSpeed * Time.unscaledDeltaTime;
					if (GameManager.activeCursorEntry.name != "Arrow")
					{
						GameManager.cursorEntriesDict["Arrow"].SetAsActive ();
						GameManager.activeCursorEntry.rectTrs.position = GameManager.cursorEntriesDict["Default"].rectTrs.position;
					}
					GameManager.activeCursorEntry.rectTrs.up = moveInput;
					GameManager.Instance.worldMapMoveViewTutorialDialog.gameObject.SetActive(false);
				}
				else if (GameManager.activeCursorEntry.name != "Default")
				{
					GameManager.cursorEntriesDict["Default"].SetAsActive ();
					GameManager.activeCursorEntry.rectTrs.position = GameManager.cursorEntriesDict["Arrow"].rectTrs.position;
				}
				if (Obelisk.playerIsAtObelisk)
					HandleFastTravel ();
			}
		}

		bool interactInput;
		bool previousInteractInput;
		public void HandleFastTravel ()
		{
			interactInput = InputManager.GetInteractInput(MathfExtensions.NULL_INT);
			foreach (Obelisk obelisk in Obelisk.instances)
			{
				if (obelisk.Found)
				{
					if (fastTravelToObelisk != obelisk && obelisk.worldMapIcon.collider.bounds.ToRect().Contains(worldMapCamera.camera.ScreenToWorldPoint(GameManager.activeCursorEntry.rectTrs.position)))
					{
						if (fastTravelToObelisk != null)
							fastTravelToObelisk.worldMapIcon.Unhighlight ();
						fastTravelToObelisk = obelisk;
						fastTravelToObelisk.worldMapIcon.Highlight ();
						break;
					}
				}
			}
			if (fastTravelToObelisk != null && interactInput && !previousInteractInput)
			{
				Obelisk.playerJustFastTraveled = true;
				SaveAndLoadManager.saveData.spawnPosition = new _Vector2(fastTravelToObelisk.worldMapIcon.collider.bounds.center);
				SaveAndLoadManager.Save (SaveAndLoadManager.currentAccountIndex);
				Close ();
				GameManager.Instance.LoadGameScenes ();
			}
			previousInteractInput = interactInput;
		}

		public void Open ()
		{
			if (WorldMap.Instance != this)
			{
				WorldMap.instance.Open ();
				return;
			}
			if (isOpen)
				return;
			canControlCamera = false;
			isOpen = true;
			GameManager.Instance.PauseGame (true);
			for (int i = 0; i < worldMapIcons.Count; i ++)
			{
				WorldMapIcon worldMapIcon = worldMapIcons[i];
				foreach (Vector2Int position in worldMapIcon.cellBoundsRect.allPositionsWithin)
				{
					if (!worldMapIcon.onlyMakeIfExplored || SaveAndLoadManager.saveData.exploredCellPositions.Contains(new _Vector2Int(position)))
						worldMapIcon.MakeIcon ();
				}
			}
			unexploredTilemap.gameObject.SetActive(true);
			HashSet<_Vector2Int> exploredCellPositionsSinceLastTimeOpened = new HashSet<_Vector2Int>(SaveAndLoadManager.saveData.exploredCellPositions);
			foreach (_Vector2Int exploredCellPositionAtLastTimeOpened in exploredCellPositionsAtLastTimeOpened)
				exploredCellPositionsSinceLastTimeOpened.Remove(exploredCellPositionAtLastTimeOpened);
			for (int i = 0; i < tilemaps.Length; i ++)
			{
				Tilemap worldMapTilemap = tilemaps[i];
				worldMapTilemap.gameObject.SetActive(true);
			}
			foreach (_Vector2Int exploredCellPositionSinceLastTimeOpened in exploredCellPositionsSinceLastTimeOpened)
			{
				unexploredTilemap.SetTile(exploredCellPositionSinceLastTimeOpened.ToVec3Int(), null);
				for (int i = 0; i < tilemaps.Length; i ++)
				{
					Tilemap worldTilemap = World.Instance.tilemaps[i];
					Tilemap worldMapTilemap = tilemaps[i];
					TileBase tile = worldTilemap.GetTile(exploredCellPositionSinceLastTimeOpened.ToVec3Int());
					if (tile != null)
					{
						worldMapTilemap.SetTile(exploredCellPositionSinceLastTimeOpened.ToVec3Int(), tile);
						worldMapTilemap.SetTransformMatrix(exploredCellPositionSinceLastTimeOpened.ToVec3Int(), worldTilemap.GetTransformMatrix(exploredCellPositionSinceLastTimeOpened.ToVec3Int()));
					}
				}
			}
			worldMapCamera.trs.position = Player.instance.trs.position.SetZ(worldMapCamera.trs.position.z);
			worldMapCamera.gameObject.SetActive(true);
			if (GameManager.Instance.worldMapTutorialConversation.gameObject.activeSelf && GameManager.instance.worldMapTutorialConversation.updateRoutine == null)
			{
				DialogManager.Instance.StartConversation (GameManager.Instance.worldMapTutorialConversation);
				for (int i = 0; i < GameManager.Instance.worldMapTutorialConversation.dialogs.Length; i ++)
				{
					Dialog dialog = GameManager.Instance.worldMapTutorialConversation.dialogs[i];
					dialog.canvas.worldCamera = worldMapCamera.camera;
				}
			}
			if (InputManager.UsingGamepad)
			{
				GameManager.cursorEntriesDict["Default"].SetAsActive ();
				GameManager.activeCursorEntry.rectTrs.localPosition = Vector2.zero;
			}
			foreach (_Vector2Int foundWorldPieceLocation in SaveAndLoadManager.saveData.foundWorldPiecesLocations)
			{
				WorldPiece foundWorldPiece = World.instance.pieces[foundWorldPieceLocation.x, foundWorldPieceLocation.y];
				foundWorldPiece.gameObject.SetActive(true);
				Enemy[] enemies = foundWorldPiece.worldObjectsParent.GetComponentsInChildren<Enemy>();
				for (int i = 0; i < enemies.Length; i ++)
				{
					Enemy enemy = enemies[i];
					if (enemy.spriteSkin != null)
						enemy.spriteSkin.enabled = true;
				}
			}
			StopAllCoroutines();
			StartCoroutine(OpenRoutine ());
		}

		public IEnumerator OpenRoutine ()
		{
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			canControlCamera = true;
		}

		public void Close ()
		{
			if (WorldMap.Instance != this)
			{
				WorldMap.instance.Close ();
				return;
			}
			if (!isOpen)
				return;
			isOpen = false;
			exploredCellPositionsAtLastTimeOpened = new HashSet<_Vector2Int>(SaveAndLoadManager.saveData.exploredCellPositions);
			for (int i = 0; i < worldMapIcons.Count; i ++)
			{
				WorldMapIcon worldMapIcon = worldMapIcons[i];
				worldMapIcon.DestroyIcon ();
				worldMapIcon.Unhighlight ();
			}
			for (int i = 0; i < tilemaps.Length; i ++)
			{
				Tilemap worldMapTilemap = tilemaps[i];
				worldMapTilemap.gameObject.SetActive(false);
			}
			worldMapCamera.gameObject.SetActive(false);
			if (!PauseMenu.Instance.gameObject.activeSelf)
				GameManager.Instance.PauseGame (false);
			if (!InputManager.UsingGamepad)
				GameManager.cursorEntriesDict["Default"].SetAsActive ();
			else
				GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(false);
			foreach (_Vector2Int foundWorldPieceLocation in SaveAndLoadManager.saveData.foundWorldPiecesLocations)
			{
				WorldPiece foundWorldPiece = World.instance.pieces[foundWorldPieceLocation.x, foundWorldPieceLocation.y];
				if (!World.instance.activePieces.Contains(foundWorldPiece))
				{
					foundWorldPiece.gameObject.SetActive(false);
					Enemy[] enemies = foundWorldPiece.worldObjectsParent.GetComponentsInChildren<Enemy>();
					for (int i = 0; i < enemies.Length; i ++)
					{
						Enemy enemy = enemies[i];
						if (enemy.spriteSkin != null)
							enemy.spriteSkin.enabled = false;
					}
				}
			}
		}
		
		public void UpdateExplored ()
		{
			int x;
			int y;
			minCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.min).ToVec2Int();
			maxCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.max).ToVec2Int();
			if (TeleportArrow.justTeleported)
			{
				TeleportArrow.justTeleported = false;
				// minExploredCellPosition = VectorExtensions.SetToMinComponents(minExploredCellPosition, minCellPosition);
				// maxExploredCellPosition = VectorExtensions.SetToMaxComponents(maxExploredCellPosition, maxCellPosition);
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
				previousMinCellPosition = minCellPosition;
				return;
			}
			if (minCellPosition.x > previousMinCellPosition.x)
			{
				x = maxCellPosition.x;
				// if (maxCellPosition.x > maxExploredCellPosition.x)
				// 	maxExploredCellPosition.x = maxCellPosition.x;
				for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
				{
					cellPosition = new _Vector2Int(x, y);
					SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.y > previousMinCellPosition.y)
				{
					y = maxCellPosition.y;
					// if (maxCellPosition.y > maxExploredCellPosition.y)
					// 	maxExploredCellPosition.y = maxCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.y < previousMinCellPosition.y)
				{
					y = minCellPosition.y;
					// if (minCellPosition.y < minExploredCellPosition.y)
					// 	minExploredCellPosition.y = minCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
			}
			else if (minCellPosition.x < previousMinCellPosition.x)
			{
				x = minCellPosition.x;
				// if (minCellPosition.x < minExploredCellPosition.x)
				// 	minExploredCellPosition.x = minCellPosition.x;
				for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
				{
					cellPosition = new _Vector2Int(x, y);
					SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.y > previousMinCellPosition.y)
				{
					y = maxCellPosition.y;
					// if (maxCellPosition.y > maxExploredCellPosition.y)
					// 	maxExploredCellPosition.y = maxCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.y < previousMinCellPosition.y)
				{
					y = minCellPosition.y;
					// if (minCellPosition.y < minExploredCellPosition.y)
					// 	minExploredCellPosition.y = minCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
			}
			else if (minCellPosition.y > previousMinCellPosition.y)
			{
				y = maxCellPosition.y;
				// if (maxCellPosition.y > maxExploredCellPosition.y)
				// 	maxExploredCellPosition.y = maxCellPosition.y;
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					cellPosition = new _Vector2Int(x, y);
					SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.x > previousMinCellPosition.x)
				{
					x = maxCellPosition.x;
					// if (maxCellPosition.x > maxExploredCellPosition.x)
					// 	maxExploredCellPosition.x = maxCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.x < previousMinCellPosition.x)
				{
					x = minCellPosition.x;
					// if (minCellPosition.x < minExploredCellPosition.x)
					// 	minExploredCellPosition.x = minCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
			}
			else if (minCellPosition.y < previousMinCellPosition.y)
			{
				y = minCellPosition.y;
				// if (minCellPosition.y < minExploredCellPosition.y)
				// 	minExploredCellPosition.y = minCellPosition.y;
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					cellPosition = new _Vector2Int(x, y);
					SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.x > previousMinCellPosition.x)
				{
					x = maxCellPosition.x;
					// if (maxCellPosition.x > maxExploredCellPosition.x)
					// 	maxExploredCellPosition.x = maxCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.x < previousMinCellPosition.x)
				{
					x = minCellPosition.x;
					// if (minCellPosition.x < minExploredCellPosition.x)
					// 	minExploredCellPosition.x = minCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new _Vector2Int(x, y);
						SaveAndLoadManager.saveData.exploredCellPositions.Add(cellPosition);
					}
				}
			}
			previousMinCellPosition = minCellPosition;
		}

		public class _Vector2Int : ICompactable<_Vector2Int, IntRange[]>
		{
			public int x;
			public int y;

			public _Vector2Int ()
			{
			}

			public _Vector2Int (int index)
			{
				RectInt worldRect = World.instance.cellBoundsRect;
				int worldWidth = worldRect.size.x;
				Vector2Int worldPosition = worldRect.position;
				x = index % worldWidth + worldPosition.x;
				y = index / worldWidth + worldPosition.y;
			}

			public _Vector2Int (int x, int y)
			{
				this.x = x;
				this.y = y;
			}

			public _Vector2Int (Vector2Int v) : this (v.x, v.y)
			{
			}

			public static _Vector2Int operator + (_Vector2Int v, _Vector2Int v2)
			{
				return new _Vector2Int(v.x + v2.x, v.y + v2.y);
			}

			public static _Vector2Int operator + (_Vector2Int v, Vector2Int v2)
			{
				return new _Vector2Int(v.x + v2.x, v.y + v2.y);
			}

			public static _Vector2Int operator + (_Vector2Int v, int i)
			{
				return new _Vector2Int(v.ToInt() + i);
			}

			public static _Vector2Int operator - (_Vector2Int v, _Vector2Int v2)
			{
				return new _Vector2Int(v.x - v2.x, v.y - v2.y);
			}

			public static _Vector2Int operator - (_Vector2Int v, Vector2Int v2)
			{
				return new _Vector2Int(v.x - v2.x, v.y - v2.y);
			}

			public static _Vector2Int operator - (_Vector2Int v, int i)
			{
				return new _Vector2Int(v.ToInt() - i);
			}

			public static _Vector2Int operator * (_Vector2Int v, _Vector2Int v2)
			{
				return new _Vector2Int(v.x * v2.x, v.y * v2.y);
			}

			public static _Vector2Int operator * (_Vector2Int v, Vector2Int v2)
			{
				return new _Vector2Int(v.x * v2.x, v.y * v2.y);
			}

			public static _Vector2Int operator * (_Vector2Int v, int i)
			{
				return new _Vector2Int(v.ToInt() * i);
			}

			public static _Vector2Int operator / (_Vector2Int v, _Vector2Int v2)
			{
				return new _Vector2Int(v.x / v2.x, v.y / v2.y);
			}

			public static _Vector2Int operator / (_Vector2Int v, Vector2Int v2)
			{
				return new _Vector2Int(v.x / v2.x, v.y / v2.y);
			}

			public static _Vector2Int operator / (_Vector2Int v, int i)
			{
				return new _Vector2Int(v.ToInt() / i);
			}

			public int ToInt ()
			{
				_Vector2Int output = new _Vector2Int(x, y);
				RectInt worldRect = World.instance.cellBoundsRect;
				output += worldRect.position;
				return output.x + output.y * worldRect.size.x;
			}

			public Vector2 ToVec2 ()
			{
				return new Vector2(x, y);
			}

			public Vector3Int ToVec3Int ()
			{
				return new Vector3Int(x, y, 0);
			}

			public IntRange[] Compact (IEnumerable<_Vector2Int> enumerable)
			{
				List<IntRange> output = new List<IntRange>();
				int worldWidth = World.instance.cellBoundsRect.size.x;
				Vector2Int worldPosition = World.instance.cellBoundsRect.position;
				IEnumerator enumerator = enumerable.GetEnumerator();
				while (enumerator.MoveNext())
				{
					_Vector2Int cellPosition = (_Vector2Int) enumerator.Current;
					int cellIndex = cellPosition.ToInt();
					bool alreadyCompacted = false;
					for (int i = 0; i < output.Count; i ++)
					{
						IntRange range = output[i];
						if (range.Contains(cellIndex))
						{
							alreadyCompacted = true;
							break;
						}
					}
					if (!alreadyCompacted)
						output.Add(new IntRange(cellIndex, cellIndex));
				}
				return output.ToArray();
			}

			public IEnumerable<_Vector2Int> Uncompact (IntRange[] compactedObject)
			{
				List<_Vector2Int> output = new List<_Vector2Int>();
				int worldWidth = World.instance.cellBoundsRect.size.x;
				Vector2Int worldPosition = World.instance.cellBoundsRect.position;
				for (int i = 0; i < compactedObject.Length; i ++)
				{
					IntRange range = compactedObject[i];
					for (int i2 = range.min; i2 <= range.max; i2 ++)
						output.Add(new _Vector2Int(i2));
				}
				return output.ToArray();
			}
		}
	}
}