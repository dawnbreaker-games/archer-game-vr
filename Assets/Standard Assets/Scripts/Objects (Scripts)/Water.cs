﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class Water : SingletonMonoBehaviour<Water>, ICopyable
	{
		BoxCollider[] waterBoundsColliders = new BoxCollider[0];
		[HideInInspector]
		public Bounds[] waterBounds = new Bounds[0];
		public float addToLinearDrag;
		public float addToAngularDrag;
		public float subtractFromGravityScale;
		public new Collider2D collider;

		void OnValidate ()
		{
			waterBoundsColliders = GetComponentsInChildren<BoxCollider>().Remove(GetComponent<BoxCollider>());
			waterBounds = new Bounds[waterBoundsColliders.Length];
			for (int i = 0; i < waterBoundsColliders.Length; i ++)
			{
				Collider waterBoundsCollider = waterBoundsColliders[i];
				waterBounds[i] = waterBoundsCollider.GetBounds(waterBoundsCollider.GetComponent<Transform>());
			}
		}

		void OnTriggerEnter (Collider other)
		{
			WorldMapIcon worldMapIcon = other.GetComponent<WorldMapIcon>();
			if (worldMapIcon != null && worldMapIcon.isActive)
				return;
			PlatformerEntity platformerEntity = other.GetComponent<PlatformerEntity>();
			if (platformerEntity != null)
			{
				// Debug.Log("Enter");
				if (platformerEntity.canSwim && !platformerEntity.isSwimming)
					platformerEntity.StartSwimming ();
			}
			else
			{
				Arrow arrow = other.GetComponent<Arrow>();
				if (GameManager.gameModifierDict["Unslowable Pull Arrows"].isActive && (arrow as PullArrow) != null)
					return;
				arrow.rigid.drag += addToLinearDrag;
				arrow.rigid.angularDrag += addToAngularDrag;
				arrow.rigid.gravityScale -= subtractFromGravityScale;
			}
		}
		
		void OnTriggerExit (Collider other)
		{
			if (other.gameObject.layer == LayerMask.NameToLayer("Map"))
				return;
			PlatformerEntity platformerEntity = other.GetComponent<PlatformerEntity>();
			if (platformerEntity != null)
			{
				// Debug.Log("Exit");
				if (platformerEntity.canSwim && !other.IsTouchingLayers(LayerMask.GetMask("Water")))
					platformerEntity.StopSwimming ();
			}
			else
			{
				Arrow arrow = other.GetComponent<Arrow>();
				if (GameManager.gameModifierDict["Unslowable Pull Arrows"].isActive && (arrow as PullArrow) != null)
					return;
				arrow.rigid.drag -= addToLinearDrag;
				arrow.rigid.angularDrag -= addToAngularDrag;
				arrow.rigid.gravityScale += subtractFromGravityScale;
			}
		}

		public void Copy (object copy)
		{
			Water water = copy as Water;
			addToLinearDrag = water.addToLinearDrag;
			addToAngularDrag = water.addToAngularDrag;
			subtractFromGravityScale = water.subtractFromGravityScale;
			waterRects = water.waterBounds;
			collider = GetComponent<Collider2D>();
			collider.isTrigger = water.collider.isTrigger;
		}
	}
}