using TMPro;
using UnityEngine;

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class Perk : MonoBehaviour
	{
		public static Perk[] instances = new Perk[0];
		public int cost;
		public TMP_Text costText;
		public bool HasPurchased
		{
			get
			{
				return SaveAndLoadManager.GetValue<bool>(name + " has purchased", false);
			}
			set
			{
				SaveAndLoadManager.SetValue (name + " has purchased", value);
			}
		}

#if UNITY_EDITOR
		public virtual void Update ()
		{
			if (Application.isPlaying)
				return;
			costText.text = "" + cost;
		}
#endif

		public virtual void Init ()
		{
			if (HasPurchased)
				Apply ();
		}

		public virtual void Apply ()
		{
			for (int i = 0; i < instances.Length; i ++)
			{
				Perk perk = instances[i];
				perk.cost += PerksMenu.Instance.addToPerkCosts;
				perk.costText.text = "" + perk.cost;
			}
		}

		public virtual void Buy ()
		{
			if (!Obelisk.playerIsAtObelisk)
			{
				if (SaveAndLoadManager.saveData.currentMoney >= cost)
					GameManager.Instance.notificationText.text.text = "You must be at an obelisk to buy perks!";
				else
					GameManager.Instance.notificationText.text.text = "You must be at an obelisk to buy perks! You don't have enough money anyway!";
				GameManager.Instance.notificationText.Do ();
				return;
			}
			if (SaveAndLoadManager.saveData.currentMoney >= cost)
			{
				Player.instance.AddMoney (-cost);
				Player.instance.DisplayMoney ();
				HasPurchased = true;
				Apply ();
				SaveAndLoadManager.Save (SaveAndLoadManager.currentAccountIndex);
			}
			else
			{
				GameManager.Instance.notificationText.text.text = "You don't have enough money to buy that!";
				GameManager.Instance.notificationText.Do ();
			}
		}
	}
}