using TMPro;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace ArcherGame
{
	//[ExecuteInEditMode]
	public class AccountSelectMenuOption : MonoBehaviour
	{
		public RectTransform rectTrs;
		public TMP_Text accountNameText;
		public GameObject createButtonGo;
		public GameObject accountNameInputFieldGo;
		public GameObject playButtonGo;
		public GameObject startCopyButtonGo;
		public GameObject endCopyButtonGo;
		public GameObject cancelCopyButtonGo;
		public GameObject deleteButtonGo;
		public TemporaryActiveText tempActiveText;
		public CanvasGroup canvasGroup;
		public static AccountSelectMenuOption copyAccountSelectMenuOption;

		public void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rectTrs == null)
					rectTrs = GetComponent<RectTransform>();
				return;
			}
#endif
		}

		public void StartCreation ()
		{
			AccountSelectMenu.Instance.canvasGroup.interactable = false;
			foreach (AccountSelectMenuOption menuOption in AccountSelectMenu.instance.menuOptions)
			{
				if (menuOption != this)
					menuOption.canvasGroup.interactable = false;
			}
			InputField accountNameInputField = accountNameInputFieldGo.GetComponent<InputField>();
			VirtualKeyboard.Instance.outputToInputField = accountNameInputField;
			VirtualKeyboard.Instance.doneKey.invokeOnPressed.RemoveAllListeners();
			VirtualKeyboard.Instance.doneKey.invokeOnPressed.AddListener(delegate { TryCreate (accountNameInputField.text); });
			VirtualKeyboard.Instance.cancelKey.invokeOnPressed.RemoveAllListeners();
			VirtualKeyboard.Instance.cancelKey.invokeOnPressed.AddListener(delegate { CancelCreate (); });
			VirtualKeyboard.Instance.EnableInput ();
			accountNameInputField.GetType().GetField("m_AllowInput", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(accountNameInputField, true);
			accountNameInputField.GetType().InvokeMember("SetCaretVisible", BindingFlags.NonPublic | BindingFlags.InvokeMethod | BindingFlags.Instance, null, accountNameInputField, null);
			FindObjectOfType<EventSystem>().SetSelectedGameObject(accountNameInputField.gameObject);
		}

		public void CancelCreate ()
		{
			AccountSelectMenu.Instance.canvasGroup.interactable = true;
			VirtualKeyboard.Instance.DisableInput ();
			foreach (AccountSelectMenuOption menuOption in AccountSelectMenu.Instance.menuOptions)
				menuOption.canvasGroup.interactable = true;
			accountNameInputFieldGo.SetActive(false);
			createButtonGo.SetActive(true);
		}

		public void TryCreate (string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				tempActiveText.text.text = "Account name can't be empty!";
				tempActiveText.Do ();
				return;
			}
			// foreach (Account account in ArchivesManager.Accounts)
			// {
			// 	if (account.Name == name)
			// 	{
			// 		tempActiveText.text.text = "Another account has that name!";
			// 		tempActiveText.Do ();
			// 		return;
			// 	}
			// }
			VirtualKeyboard.Instance.DisableInput ();
			accountNameText.text = "Account: " + name;
			SaveAndLoadManager.saveData.name = name;
			// SaveAndLoadManager.Instance.Save ();
			accountNameInputFieldGo.SetActive(false);
			playButtonGo.SetActive(true);
			startCopyButtonGo.SetActive(true);
			deleteButtonGo.SetActive(true);
			AccountSelectMenu.Instance.canvasGroup.interactable = true;
			foreach (AccountSelectMenuOption menuOption in AccountSelectMenu.Instance.menuOptions)
				menuOption.canvasGroup.interactable = true;
		}
		
		public void Play ()
		{
			AccountSelectMenu.Instance.gameObject.SetActive(false);
			if (SaveAndLoadManager.currentAccountIndex == rectTrs.GetSiblingIndex())
				PauseMenu.Instance.Hide ();
			else if (SaveAndLoadManager.currentAccountIndex != -1)
			{
				// SaveAndLoadManager.ResetPersistantValues ();
				SaveAndLoadManager.currentAccountIndex = rectTrs.GetSiblingIndex();
				GameManager.Instance.LoadScene ("Init");
			}
			else
			{
				SaveAndLoadManager.currentAccountIndex = rectTrs.GetSiblingIndex();
				SaveAndLoadManager.Load (SaveAndLoadManager.currentAccountIndex);
				GameManager.Instance.PauseGame (false);
			}
		}

		public void StartCopy ()
		{
			copyAccountSelectMenuOption = this;
			foreach (AccountSelectMenuOption accountSelectMenuOption in AccountSelectMenu.Instance.menuOptions)
			{
				if (accountSelectMenuOption != this)
				{
					accountSelectMenuOption.endCopyButtonGo.SetActive(true);
					if (string.IsNullOrEmpty(SaveAndLoadManager.GetAccountName(accountSelectMenuOption.rectTrs.GetSiblingIndex())))
					{
						accountSelectMenuOption.createButtonGo.SetActive(false);
						accountSelectMenuOption.accountNameInputFieldGo.SetActive(false);
					}
					else
					{
						accountSelectMenuOption.playButtonGo.SetActive(false);
						accountSelectMenuOption.startCopyButtonGo.SetActive(false);
						accountSelectMenuOption.deleteButtonGo.SetActive(false);
					}
				}
			}
		}

		public void CancelCopy ()
		{
			OnStopCopy ();
		}

		public void EndCopy ()
		{
			// account = copyAccountSelectMenuOption.account;
			// account.Name += " (Copy)";
			// accountNameText.text = "Account: " + account.Name;
			// account.index = rectTrs.GetSiblingIndex();
			// SaveAndLoadManager.Instance.Save ();
			copyAccountSelectMenuOption.cancelCopyButtonGo.SetActive(false);
			OnStopCopy ();
		}

		void OnStopCopy ()
		{
			foreach (AccountSelectMenuOption accountSelectMenuOption in AccountSelectMenu.Instance.menuOptions)
			{
				if (accountSelectMenuOption != this)
				{
					accountSelectMenuOption.endCopyButtonGo.SetActive(false);
					if (string.IsNullOrEmpty(SaveAndLoadManager.GetAccountName(accountSelectMenuOption.rectTrs.GetSiblingIndex())))
						accountSelectMenuOption.createButtonGo.SetActive(true);
					else
					{
						accountSelectMenuOption.playButtonGo.SetActive(true);
						accountSelectMenuOption.startCopyButtonGo.SetActive(true);
						accountSelectMenuOption.deleteButtonGo.SetActive(true);
					}
				}
			}
		}

		public void Delete ()
		{
			SaveAndLoadManager.Delete (rectTrs.GetSiblingIndex());
			if (SaveAndLoadManager.currentAccountIndex == rectTrs.GetSiblingIndex())
			{
				// SaveAndLoadManager.currentAccountIndex = -1;
				// SaveAndLoadManager.ResetPersistantValues ();
				GameManager.Instance.LoadGameScenes ();
			}
			AccountSelectMenu.Init ();
		}
	}
}
