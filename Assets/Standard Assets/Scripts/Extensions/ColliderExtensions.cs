using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArcherGame;

namespace Extensions
{
	public static class ColliderExtensions
	{
		public static Bounds GetBounds (this Collider collider)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return collider.GetRect(collider.GetComponent<Transform>());
#endif
			return collider.bounds.ToRect().SetPositiveSize();
		}

		public static Bounds GetBounds (this Collider collider, Transform trs)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				Bounds output = new Bounds();
				BoxCollider boxCollider = collider as BoxCollider;
				if (boxCollider != null)
					output = Bounds.MinMaxBounds(trs.position.x + boxCollider.offset.x * trs.lossyScale.x - boxCollider.size.x / 2 * trs.lossyScale.x - boxCollider.edgeRadius * trs.lossyScale.x, trs.position.y + boxCollider.offset.y * trs.lossyScale.y - boxCollider.size.y / 2 * trs.lossyScale.y - boxCollider.edgeRadius * trs.lossyScale.y, trs.position.x + boxCollider.offset.x * trs.lossyScale.x + boxCollider.size.x / 2 * trs.lossyScale.x + boxCollider.edgeRadius * trs.lossyScale.x, trs.position.y + boxCollider.offset.y * trs.lossyScale.y + boxCollider.size.y / 2 * trs.lossyScale.y + boxCollider.edgeRadius * trs.lossyScale.y);
				else
					throw new NotImplementedException();
				return output.SetPositiveSize();
			}
#endif
			return collider.bounds.SetPositiveSize();
		}

		public static Vector2 GetCenter (this Collider collider)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return collider.GetCenter(collider.GetComponent<Transform>());
#endif
			return collider.bounds.ToRect().SetPositiveSize().center;
		}

		public static Vector2 GetCenter (this Collider collider, Transform trs)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				BoxCollider boxCollider = collider as BoxCollider;
				if (boxCollider != null)
					return new Vector2(trs.position.x + boxCollider.offset.x * trs.lossyScale.x, trs.position.y + boxCollider.offset.y * trs.lossyScale.y);
				else
					return collider.GetRect(trs).center;
			}
#endif
			return collider.bounds.ToRect().SetPositiveSize().center;
		}

		public static Vector2 GetSize (this Collider collider)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return collider.GetSize(collider.GetComponent<Transform>());
#endif
			return collider.bounds.ToRect().SetPositiveSize().size;
		}

		public static Vector2 GetSize (this Collider collider, Transform trs)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				BoxCollider boxCollider = collider as BoxCollider;
				if (boxCollider != null)
					return new Vector2((boxCollider.size.x + boxCollider.edgeRadius * 2) * trs.lossyScale.x, (boxCollider.size.y + boxCollider.edgeRadius * 2) * trs.lossyScale.y);
				else
					return collider.GetRect(trs).size;
			}
#endif
			return collider.bounds.ToRect().SetPositiveSize().size;
		}
	}
}